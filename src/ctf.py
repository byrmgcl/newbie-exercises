# Copyright (C) 2015 by Bayram Güçlü

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
All this things simple but useful for me.
"""

import os
import os.path as path

from glob import glob
from shutil import copy

CURRENT_DIRECTORY = path.realpath(os.curdir)

def echo(message: "string"=""):
    """The function echoes message to terminal or emacs echo area
    by using M-!"""
    try:
        os.system("echo " + message)
    except ValueError:
        print("Please type string, nothing else!")
    except Exception:
        raise


def create_tern_file():
    """This function copies a ".tern-project" file
    to current directory if it does not exist
    and current directory contains a .js file.
    """
    source = path.join(path.expanduser("~"), "Templates", ".tern-project")

    # check whether .tern-project file exist
    tfile = path.isfile(path.join(CURRENT_DIRECTORY, ".tern-project"))

    jsfiles = glob(path.join(CURRENT_DIRECTORY, "*.js"))
    if  jsfiles and not tfile:
        try:
            copy(source, CURRENT_DIRECTORY)
            echo(".tern-project file has been created.")

        except FileNotFoundError as err:
            echo("File not found: {}".format(err.filename))

        except Exception:
            echo("Something\\'s gone wrong!")
            raise

    elif jsfiles and tfile:
        echo(".tern-project file already exists.")

    else:
        echo("No JavaScript files in this directory.")


if __name__ == "__main__":
    create_tern_file()
