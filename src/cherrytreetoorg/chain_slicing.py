"""
This module includes ChainSlicing class
that aims to decrease typing and
increase functionality for slicing.
"""
from collections import OrderedDict
from itertools import combinations


class ChainSlicing(object):
    """
    Basic Usage:

    """
    def __init__(self, obj_name: str='data'):
        self._data = OrderedDict() # that reserves all slicing datas.
        self.obj_name = obj_name
        self._obj = None

    def add_args(self, *args: tuple):
        slices = ""
        for i in range(len(args)):
            slices += '[{arg}]'.format(arg=repr((args[i])))
        self._append('slices', slices)
        return self

    def add_args_for_formattting(self, *args: tuple):
        slices = ""
        for i in range(len(args)):
            slices += '[{arg}]'.format(arg=str(args[i]))
        self._append('slices_for_formatting', slices)
        return self

    def produce_slice(self, arg: str='', depth: int=2):
        slices = ""
        for i in range(depth):
            slices += '[{arg}]'.format(
                arg=repr(arg + str(i)))
        self._append('slicing_products', slices)
        return self

    def produce_slice_for_formatting(self, arg: str, depth: int):
        slices = ""
        for i in range(depth):
            slices += '[{arg}]'.format(
                arg=str(arg[:-1] + str(i) + '}') if # This is for argument
                '[' not in arg and '{' in arg else
                str(arg[:-2] + str(i) + ']}')) # This is for list
        self._append('slicing_products', slices)
        return self

    def chain(self):
        obj_name = self.obj_name
        for key in self._data.copy():
            if self._data.get(key):
                for value in self._data.get(key):
                    obj_name += str(value)
                del self._data[key]
        self._sliced_obj = obj_name
        return self

    def eval(self):
        value = eval(repr(self._sliced_obj))
        self._sliced_obj = None
        return value

    def show(self):
        return self._sliced_obj

    def _append(self, key: str, value: str):
        self._data.setdefault(key, [])
        self._data[key].append(value)

    # def __str__(self):
    #     return self.slices


def _produce_combinations_obj(n: int):
    product_obj = combinations(range(n), n)
    return product_obj

def produce_slice_via_combinations(*args, n: int, depth: int, name: str='names'):
    chainslicing = ChainSlicing()
    counter = 0
    slices = []
    for pro_obj in _produce_combinations_obj(n):
        if counter < n:
            frmt = [name + '[' + str(pro_obj[i]) + ']'
                    for i in range(depth)]
            if args:
                slicing_products = chainslicing.produce_slice_for_formatting(
                    '{0[]}', depth)
                for i in range(len(args)):
                    slicing_products.add_args(args[i])
                slicing_products = slicing_products.chain().show()
                slices.append(slicing_products.format(frmt))
            else:
                slicing_products = chainslicing.produce_slice_for_formatting(
                    '{0[]}', depth).chain().show()
                slices.append(slicing_products.format(frmt))
            counter += 1
    return slices


if __name__ == '__main__':
    # print(*produce_slice_via_combinations(n=0, depth=1))
    print(produce_slice_via_combinations(n=1, depth=0)[0]) # data
    print(produce_slice_via_combinations(n=2, depth=1)[0]) # data[names[0]]
    print(produce_slice_via_combinations(n=3, depth=2)[0]) # data[names[0]][names[1]
    print(produce_slice_via_combinations('content', n=4, depth=3)[0]) # data[names[0]][names[1]]
    chaining = ChainSlicing()
    a = chaining.produce_slice_for_formatting('arg[]', 3).chain()
    print(a.show())

    ## print(*itertools.product(range(1), range(1,2), range(2,3)))
    ## (0, 1, 2)
    ## print(*itertools.product(range(1), range(1,2), range(2,3), range(3,4)))
    ## (0, 1, 2, 3)
    ## print(*itertools.combinations(range(3), 3))
    ## (0, 1, 2)
    ## print(*itertools.combinations(range(4), 4))
    ## (0, 1, 2, 3)
