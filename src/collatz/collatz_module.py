# Copyright (C) 2015 by Bayram Güçlü

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Sayi(int):
    """
    Bu sınıf sayının tek mi çift mi olduğunu denetler.
    """
    def __init__(self, sayi):
        super(Sayi, self).__init__()
        self.sayi = sayi

    @property
    def sayi(self):
        return self._sayi

    @sayi.setter
    def sayi(self, x):
        if not isinstance(x, int):
            raise TypeError("Lütfen sayı giriniz!")
        self._sayi = x

    def tek_mi_cift_mi(self: "Sayi") -> "bool":
        """Eğer sayı tek ise True,
        çift ise False döndürür."""
        return True if self % 2 else False


class Collatz(Sayi):
    """
    Collatz Kuramı:
    Eğer sayı çift ise defalarca 2'ye böldüğümüzde,
    tek ise 3 katını alıp 1 ekleyerek defalarca 2'ye böldüğümüzde,
    sonunda elde edeceğimiz sayı 1 olacaktır.
    """
    def __init__(self, sayi: "int"=100) -> "1":
        super(Collatz, self).__init__(sayi)

    @property
    def kalan(self):
        return self._ikiye_bol()

    def _ikiye_bol(self):
        if self.tek_mi_cift_mi():
            kalan = self._tek_sayiyi_bol()
        else:
            kalan = self._cift_sayiyi_bol()
        return kalan

    def _tek_sayiyi_bol(self):
        sayi = self.sayi*3 + 1
        while sayi != 1:
            sayi //= 2
        return sayi

    def _cift_sayiyi_bol(self):
        sayi = self.sayi
        while sayi != 1:
            sayi //= 2
        return sayi

    def __repr__(self):
        return str(self.kalan)

    def __str__(self):
        return str(self.kalan)
