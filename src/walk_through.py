# Copyright (C) 2015 by Bayram Güçlü

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
I use Debian. All docs under the /usr/share/doc/ path in Debian.
This path includes some programming docs and some Debian specific
Readme files under the specific directories, so looking for docs
very boring thing because so many directories exist under the
/usr/share/doc/. I just want to collect directory that includes
pdf, html etc. files. So I wrote this script.
"""
import os
import shutil
import sys
import argparse


KNOWN_EXTENSIONS = ('.pdf', '.html', '.ps')


class DocumentFinder(object):
    def __init__(self, src: str, dest: str):
        self.src = src if not src.endswith('/') else src[:-1]
        self.dest = dest
        self.out = set()

    def search(self, suffix: str):
        """This function looks for file under given
        path that includes given suffix. Returns
        set that includes tuple pairs."""
        # Iterate along given path. Every iteration returns tuple triples.
        # We don't need second value thus we wrote _.
        for root, _, files in os.walk(self.src):
            for file_ in files: # Iterate throughly files.
                # File have given suffix?
                if suffix == str(os.path.splitext(file_)[1]):
                    split = os.path.split
                    while root != self.src:
                        root, tail = split(root)
                    self.out.add((root, tail))
                    break # because we don't need other files.
                    # We only need path now. After we will copy all files.

    def copy(self):
        """This function copies files iteratively."""
        for root, tail in set(self.out):
            src = os.path.join(root, tail)
            shutil.copytree(src, self.dest + tail, symlinks=True)
        self.out = set()

    def link(self):
        for root, tail in self.out:
            src = os.path.join(root, tail)
            os.system('ln -s {src} {dest}'.format(src=src, dest=self.dest))
        self.out = set()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i', '--input', help='add input for searching.')
    parser.add_argument(
        '-o', '--output', help='add output for copying.')
    parser.add_argument(
        '-s', '--suffix', help='add suffix for searching.')
    parser.add_argument(
        '-c', '--copy',
        help='If True then copy all finding output else only create links.'
    )
    args = parser.parse_args()

    cur_path = os.path.abspath(os.curdir)

    if args.input:
        if not args.output:
            try:
                os.mkdir('output')

            except FileExistsError:
                sys.exit('Directory already exist!')

        documentfinder = DocumentFinder(
            args.input,
            os.path.join(
                cur_path, 'output' if not args.output else args.output))
    else:
        print('Please provide input path!')

    condition1 = args.input and args.output and args.suffix
    condition2 = args.input and not (args.output and args.suffix)

    if args.output:
        documentfinder.dest = args.output

    if condition1:
        print("Looking for {} ...".format(args.suffix))
        if args.copy:
            documentfinder.search(args.suffix)
            print("Copying all {} files ...".format(args.suffix))
            documentfinder.copy()
        else:
            documentfinder.search(args.suffix)
            print("Creating links for all {} files ...".format(args.suffix))
            documentfinder.link()
    elif condition2:
        for ext in KNOWN_EXTENSIONS:
            print("Looking for {} ...".format(ext))
            path = os.path.join(cur_path, 'output', ext[1:])
            os.makedirs(path)
            documentfinder.dest = path
            if args.copy:
                documentfinder.search(ext)
                print("Copying all {} files ...".format(ext))
                documentfinder.copy()
            else:
                documentfinder.search(ext)
                print("Creating links for all {} files ...".format(ext))
                documentfinder.link()


if __name__ == '__main__':
    main()
