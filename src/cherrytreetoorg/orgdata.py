"""ORG"""

class ORG(object):
    def __init__(self):
        self._structure = {
            'heading':              '* {text}',
            'bold_text':            '*{text}*',
            'italic_text':          '/{text}/',
            'code_text':            '={text}=',
            'underlined_text':      '_{text}_',
            'strike_through_text':  '+{text}+',
            'verbatim_text':        '~{text}~'
            }
        self._head_struct = self._structure['heading'].split()

    def fl_heading(self, text: str) -> str:
        """First level heading."""
        return self._structure['heading'].format(text=text)

    def sl_heading(self, text: str) -> str:
        """Second level heading."""
        return self._head_struct[0]*2 + ' ' + self._head_struct[1].format(text=text)

    def tl_heading(self, text: str) -> str:
        """Third level heading."""
        return self._head_struct[0]*3 + ' ' + self._head_struct[1].format(text=text)

    def bold(self, text: str) -> str:
        return self._structure['bold_text'].format(text=text)

    def italic(self, text: str) -> str:
        return self._structure['italic_text'].format(text=text)

    def code(self, text: str) -> str:
        return self._structure['code_text'].format(texxt=text)

    def underlined(self, text: str) -> str:
        return self._structure['underlined_text'].format(text=text)

    def verbatim(self, text: str) -> str:
        return self._structure['verbatim_text'].format(text=text)

    def strike_through(self, text: str) -> str:
        return self._structure['strike_through_text'].format(text)

a = ORG()
print(a.sl_heading("bayram"))
print(a.bold("bayram"))
print(a.italic("bayram"))
