"""Cherrytree"""
import xml.etree.ElementTree as etree
from collections import OrderedDict

import chain_slicing as cs

chain_slicing = cs.ChainSlicing()

data = OrderedDict()
def parse(root, names=None, i=0, depth=0):
    if not names:
        names = []
    root1 = root

    for node in root:
        if node.tag == 'node':
            node_name = node.get('name')
            names.append(node_name)
            eval(cs.produce_slice_via_combinations(
                n=i+1, depth=depth)[0]).setdefault(
                names[i],
                OrderedDict(content=[]))
            i += 1
            depth += 1
            parse(node, names, i, depth)

        if node.tag == 'rich_text':
            text = node.text
            weight = node.get('weight')
            scale = node.get('scale')
            style = node.get('style')
            link = node.get('link')
            isCode = node.get('family') == 'monospace'
            if (isCode):
                print(text, weight, scale, style, link, isCode)
            eval(cs.produce_slice_via_combinations(
                'content', n=i, depth=depth)[0]).append((text, weight, scale, style, link, isCode))

            # TODO: tables, codes and images



    # else:
    #     names = names[:]
    #     i = 1
    #     depth = 0
    #     parse(node[], names, i)

if __name__ == '__main__':
    tree = etree.parse('/home/byrmgcl/Files/notlar/cherrytree/Notlar.ctd')
    root = tree.getroot()
    parse(root)
    print(data, file=open('/home/byrmgcl/dev/non-git/emacs/cherrytreetoorg/cherrytree.pydict', "a"))
