#!/bin/bash

function start_redshift () {
    killall -q redshift
    SECONDS=0
    while :
    do
        sleep 60
        let "hours=SECONDS/3600"
        let "minutes=(SECONDS%3600)/60"
        let "seconds=(SECONDS%3600)%60"
        if (( $minutes > $1 )) ; then
            redshift-gtk -l 36:28 -m randr
            break
        fi
    done
}

start_redshift $1
