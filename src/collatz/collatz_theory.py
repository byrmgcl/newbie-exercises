# Copyright (C) 2015 by Bayram Güçlü

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import collatz_module as cm

def collatz_kurami(sayi1: "int", sayi2: "int") -> "iterable":
    # Kullanıcı sayı aralığını beklediğimiz gibi girmeyebilir.
    ilk_sayi, ikinci_sayi = min(sayi1, sayi2), max(sayi1, sayi2)
    collatz_generator = (cm.Collatz(i) for i in range(ilk_sayi, ikinci_sayi))
    return collatz_generator


def hesapla() -> "None":
    print("Collatz Kuramı:",
          "Eğer sayı çift ise defalarca 2'ye böldüğümüzde,",
          "tek ise 3 katını alıp 1 ekleyerek defalarca 2'ye böldüğümüzde,",
          "sonunda elde edeceğimiz sayı 1 olacaktır.",
          "Burada girilen sayı aralığındaki bütün sayılar",
          "tek tek defalarca ikiye bölünerek kalan sayı yazdırılacaktır.",
          "Lütfen bir sayı aralığı giriniz!", sep="\n")

    sayi1 = input("İlk sayı: ")
    sayi2 = input("İkinci sayı: ")


    try:
        sayi1 = int(sayi1)
        sayi2 = int(sayi2)
    except ValueError:
        print("Lütfen harf değil sayı giriniz!")

    for i in collatz_kurami(sayi1, sayi2):
        print(i, end=" ")


if __name__ == '__main__':
    hesapla()
